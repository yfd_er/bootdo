package com.bootdo.api;

import com.bootdo.system.vo.UserVO;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserApiController {

    @ApiOperation("新增用户接口")
    @PostMapping("/add")
    public boolean addUser(@RequestBody UserVO user) {
        return false;
    }
    @GetMapping("/find/{id}")
    public UserVO findById(@PathVariable("id") int id) {
        return new UserVO();
    }
    @PutMapping("/update")
    public boolean update(@RequestBody UserVO user) {
        return true;
    }
    @DeleteMapping("/delete/{id}")
    public boolean delete(@PathVariable("id") int id) {
        return true;
    }
}